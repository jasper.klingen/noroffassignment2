import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        difficulty: 'easy',
        numberOfQuestions: 10,
        selectedCatagory: 9,
        catagoryList: [],
        answers: [{ 
            id:0,
            Text:""
        }],
        activeScreen: "home",
        questionList: [],
        currentQuestionNum: 0,
        

    },
    mutations: {
        changeDifficulty: (state, payload) => {
            state.difficulty = payload
        },
        setNumberOfQuestions: (state, payload) => {
            try{
                state.numberOfQuestions = parseInt(payload)
            }
            catch(error){
                console.error("give a number" + error)
            }
        },
        changeCatagory: (state, payload) => {
            state.selectedCatagory = payload
        },
        changeCatagoryList: (state, payload) => {
            state.catagoryList = payload
        },
        changeActiveScreen: (state, payload) => {
            state.activeScreen = payload
        },
        changeQuestionList: (state, payload) => {
            state.questionList = payload
        },
        changeCurrentQuestionNum: (state, payload) => {
            state.currentQuestionNum = payload
        }
    },
    getters: {

    },
    actions: {
        async getCatagory({ commit}){
            try {
                const response = await fetch(`https://opentdb.com/api_category.php`)
                const responseList = await response.json()
                const catagoryList = await responseList.trivia_categories
                commit('changeCatagoryList', catagoryList)

               
            }
            catch(error){
                console.log("Something went wrong with requesting the catagories" + error);
            }
        },
        async getListOfQuestions({commit}){
            try {
                const URL = `https://opentdb.com/api.php?amount=${this.state.numberOfQuestions}&category=${this.state.selectedCatagory}&difficulty=${this.state.difficulty}`
                const response = await fetch(URL)
                const responseList = await response.json()
                commit('changeQuestionList', responseList.results)
            } catch (error) {
                console.log("something went wrong in the api request of the questions" + error)
            }
        } 
    }
})